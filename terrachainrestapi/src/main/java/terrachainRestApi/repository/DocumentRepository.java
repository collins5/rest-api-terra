package terrachainRestApi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import terrachainRestApi.entity.Document;

@Repository
public interface DocumentRepository extends JpaRepository<Document, Long> {

}
