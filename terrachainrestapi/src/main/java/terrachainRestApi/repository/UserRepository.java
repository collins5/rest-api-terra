package terrachainRestApi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import terrachainRestApi.entity.User;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    List<User> findByFirstName(String firstname);

    User findByFirstNameAndLastName(String firstname, String lastname);

    List<User> findByFirstNameOrLastName(String firstName, String lastName);

    List<User> findByFirstNameIn(List<String> firstNames);

    List<User> findByFirstNameContains(String firstName);

    List<User> findByFirstNameStartsWith(String firstName);

    List<User> findByFirstNameEndsWith(String firstName);

    List<User> findByAddress_City(String city);

    Optional<User> findByEmail(String email);

    Optional<User> findByUsernameOrEmail(String username, String email);

    Optional<User> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

}
