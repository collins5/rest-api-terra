package terrachainRestApi.dtos;

import lombok.Data;
import terrachainRestApi.entity.Address;

@Data
public class SignUpDTO {
    private String name;
    private String lastName;
    private String username;
    private String email;
    private String password;
//    private Address address;
}
