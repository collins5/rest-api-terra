package terrachainRestApi.dtos;

public class JWTAuthDTO {

    private String accessToken;
    private String tokenTyp = "Bearer";

    public JWTAuthDTO(final String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenTyp() {
        return tokenTyp;
    }

    public void setAccessToken(final String accessToken) {
        this.accessToken = accessToken;
    }

    public void setTokenTyp(final String tokenTyp) {
        this.tokenTyp = tokenTyp;
    }
}
