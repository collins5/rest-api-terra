package terrachainRestApi.dtos;

import lombok.Getter;
import lombok.Setter;
import terrachainRestApi.entity.Document;

import java.util.Date;

@Getter
@Setter
public class DocumentDTO {

    private Long id;

    private String name;
    private Date entryDate;
    private Date updateDate;
    private Date expirationDate;
    private Boolean locked;
    private String comment;

    public DocumentDTO(final Document document) {
        this.id = document.getId();
        this.name = document.getName();
        this.entryDate = document.getEntryDate();
        this.updateDate = document.getUpdateDate();
        this.expirationDate = document.getExpirationDate();
        this.locked = document.getLocked();
        this.comment = document.getComment();
    }
}
