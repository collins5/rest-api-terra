package terrachainRestApi.dtos;

import lombok.Getter;
import lombok.Setter;
import terrachainRestApi.entity.Document;
import terrachainRestApi.entity.User;

import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
public class UserDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private String username;
    private String email;
    private String password;
    private String street;
    private String city;
    private String provinz;
    private String country;

    private Set<DocumentDTO> documentDTOS;

    public UserDTO(User user) {

        this.id = user.getId();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.password = user.getPassword();

        this.street = user.getAddress().getStreet();
        this.city = user.getAddress().getCity();
        this.provinz = user.getAddress().getProvinz();
        this.country = user.getAddress().getCountry();

        if (user.getDocumentSet() != null) {
            documentDTOS = new HashSet<>();
            for (Document document : user.getDocumentSet()) {
                documentDTOS.add(new DocumentDTO(document));
            }
        }
    }

}
