package terrachainRestApi.service;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import terrachainRestApi.entity.Address;
import terrachainRestApi.entity.Document;
import terrachainRestApi.entity.User;
import terrachainRestApi.repository.AddressRepository;
import terrachainRestApi.repository.DocumentRepository;
import terrachainRestApi.repository.UserRepository;
import terrachainRestApi.requests.CreateDocumentRequest;
import terrachainRestApi.requests.CreateUserRequest;
import terrachainRestApi.requests.InQueryRequest;
import terrachainRestApi.requests.UpdateRequestUser;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class UserService {

    final UserRepository userRepository;
    final AddressRepository addressRepository;

    final DocumentRepository documentRepository;

    public UserService(final UserRepository userRepository,
                       final AddressRepository addressRepository,
                       final DocumentRepository documentRepository) {
        this.userRepository = userRepository;
        this.addressRepository = addressRepository;
        this.documentRepository = documentRepository;
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    public User createUser(CreateUserRequest createUserRequest) {
        User user = new User(createUserRequest);

        Address address = new Address();
        address.setStreet(createUserRequest.getStreet());
        address.setCity(createUserRequest.getCity());
        address.setProvinz(createUserRequest.getProvinz());
        address.setCountry(createUserRequest.getCountry());

        address = addressRepository.save(address);

        user.setAddress(address);
        user = userRepository.save(user);

        Set<Document> documents = new HashSet<>();

        if (createUserRequest.getDocuments() != null) {

            for (CreateDocumentRequest documentRequest :
                    createUserRequest.getDocuments()) {
                Document document = new Document();
                document.setName(documentRequest.getName());
                document.setEntryDate(documentRequest.getEntryDate());
                document.setUpdateDate(documentRequest.getUpdateDate());
                document.setExpirationDate(documentRequest.getExpirationDate());
                document.setLocked(documentRequest.getLocked());
                document.setComment(documentRequest.getComment());
                document.setUser(user);

                documents.add(document);
            }

            documentRepository.saveAll(documents);
        }

        user.setDocumentSet(documents);
        return user;
    }

    public User updateUser(UpdateRequestUser updateRequestUser) {
        User optionalUser = userRepository.
                findById(updateRequestUser.getId()).get();
        if (updateRequestUser.getFirstName() != null &&
                !updateRequestUser.getFirstName().isEmpty()) {
            optionalUser.setFirstName(updateRequestUser.getFirstName());
        }
        return userRepository.save(optionalUser);
    }

    public String deleteUser(Long id) {
        userRepository.deleteById(id);
        return "User has been deleted successfully";
    }

    public List<User> getByFirstName(String firstName) {
        return userRepository
                .findByFirstName(firstName);
    }

    public User getByFirstNameAndLastName(String firstName, String lastName) {
        return userRepository
                .findByFirstNameAndLastName(firstName, lastName);
    }

    public List<User> getByFirstNameOrLastName(String firstName, String lastName) {
        return userRepository
                .findByFirstNameOrLastName(firstName, lastName);
    }

    public List<User> getByFirstNameIn(final InQueryRequest inQueryRequest) {
        return userRepository
                .findByFirstNameIn(inQueryRequest.getFirstNames());
    }

    public List<User> getAllUserWithPagination(final int pageNo, final int pageSize) {
        Pageable pageable = PageRequest.of(pageNo, pageSize);
        return userRepository
                .findAll(pageable).getContent();
    }

    public List<User> getAllUserWithSorting() {
        Sort sort = Sort.by(Sort.Direction.ASC,
                "firstName", "lastName", "userName");
        return userRepository.findAll(sort);
    }

    public List<User> like(final String firstName) {
        return userRepository
                .findByFirstNameContains(firstName);
    }

    public List<User> startsWith(final String firstName) {

        return userRepository
                .findByFirstNameStartsWith(firstName);
    }

    public List<User> endsWith(final String firstName) {
        return userRepository
                .findByFirstNameEndsWith(firstName);
    }

    public List<User> getByCity(final String city) {
        return userRepository
                .findByAddress_City(city);
    }
}
