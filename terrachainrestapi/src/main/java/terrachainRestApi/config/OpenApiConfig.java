package terrachainRestApi.config;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public GroupedOpenApi publicApi() {
        return GroupedOpenApi.builder()
                .group("Terrachain-api")
                .pathsToMatch("/api/**")
                .build();
    }

    @Bean
    public OpenAPI apiDoc() {
        return new OpenAPI()
                .info(new Info()
                        .title("Terrachain Api")
                        .description("API Terrachain")
                        .version("v1.0.0")
                        .license(
                                new License()
                                        .name("TERRA")
                                        .url("filparty.com")
                        ))
                .externalDocs(new ExternalDocumentation()
                        .description("REST API SpringBoot")
                        .url("http://localhost:5000/api/"));
    }
}
