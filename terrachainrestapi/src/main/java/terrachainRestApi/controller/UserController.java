package terrachainRestApi.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import terrachainRestApi.Utils.TERRA_Constants;
import terrachainRestApi.dtos.UserDTO;
import terrachainRestApi.entity.User;
import terrachainRestApi.requests.CreateUserRequest;
import terrachainRestApi.requests.InQueryRequest;
import terrachainRestApi.requests.UpdateRequestUser;
import terrachainRestApi.service.UserService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/users/")
public class UserController {

    final UserService userService;

    public UserController(final UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/getAll")
    public List<UserDTO> getAllUsers() {
        List<User> userEntityList = userService.getAllUser();
        List<UserDTO> userDTOList = new ArrayList<>();

        userEntityList.stream().forEach(user -> {
            userDTOList.add(new UserDTO(user));
        });

        return userDTOList;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create")
    public UserDTO createUser(@Valid @RequestBody CreateUserRequest createUserRequest) {
        User user = userService.createUser(createUserRequest);
        return new UserDTO(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping("/update")
    public UserDTO updateUser(@Valid @RequestBody UpdateRequestUser updateRequestUser) {
        User user = userService.updateUser(updateRequestUser);
        return new UserDTO(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public String deleteUser(@PathVariable("id") Long id) {
        return userService.deleteUser(id);
    }

    @GetMapping("/getByFirstName/{firstName}")
    public List<UserDTO> getByFirstName(@PathVariable("firstName") String firstName) {
        List<User> userList = userService.getByFirstName(firstName);
        List<UserDTO> userDTOList = new ArrayList<>();
        userList.stream().forEach(user -> {
            userDTOList.add(new UserDTO(user));
        });
        return userDTOList;
    }

    @GetMapping("getByFirstNameAndLastName/{firstName}/{lastName}")
    public UserDTO getByFirstNameAndLastName(@PathVariable("firstName") String firstName,
                                             @PathVariable String lastName) {
        User user = userService
                .getByFirstNameAndLastName(firstName, lastName);
        return new UserDTO(user);
    }

    @GetMapping("getByFirstNameOrLastName/{firstName}/{lastName}")
    public List<UserDTO> getByFirstNameOrLastName(@PathVariable("firstName") String firstName,
                                                  @PathVariable("lastName") String lastName) {
        List<User> users = userService.getByFirstNameOrLastName(firstName, lastName);
        List<UserDTO> userDTOS = new ArrayList<>();
        users.stream().forEach(user -> {
            userDTOS.add(new UserDTO(user));
        });
        return userDTOS;
    }

    @GetMapping("getByFirstNameIn")
    public List<UserDTO> getByFirstNameIn(@RequestBody InQueryRequest inQueryRequest) {
        List<User> users = userService.getByFirstNameIn(inQueryRequest);
        List<UserDTO> userDTOS = new ArrayList<>();
        users.stream().forEach(user -> {
            userDTOS.add(new UserDTO(user));
        });
        return userDTOS;
    }

    @GetMapping("getAllWithPagination")
    public List<UserDTO> getAllUserWithPagination(
            @RequestParam(value = "pageNo",
                    defaultValue = TERRA_Constants.DEFAULT_PAGE_NUMBER,
                    required = false) int pageNo,
            @RequestParam(value = "pageSize",
                    defaultValue = TERRA_Constants.DEFAULT_PAGE_SIZE,
                    required = false) int pageSize) {

        List<User> users = userService.getAllUserWithPagination(pageNo, pageSize);
        List<UserDTO> userDTOS = new ArrayList<>();
        users.stream().forEach(user -> {
            userDTOS.add(new UserDTO(user));
        });

        return userDTOS;
    }

    @GetMapping("getAllWithSorting")
    public List<UserDTO> getAllUserWithSorting() {
        List<User> users = userService.getAllUserWithSorting();
        List<UserDTO> userDTOS = new ArrayList<>();

        users.stream().forEach(user -> {
            userDTOS.add(new UserDTO(user));
        });

        return userDTOS;
    }

    @GetMapping("like/{firstName}")
    public List<UserDTO> like(@PathVariable String firstName) {
        List<User> users = userService.like(firstName);
        List<UserDTO> userDTOS = new ArrayList<>();
        users.stream().forEach(user -> {
            userDTOS.add(new UserDTO(user));
        });
        return userDTOS;
    }

    @GetMapping("startsWith/{firstName}")
    public List<UserDTO> startsWith(@PathVariable String firstName) {
        List<User> users = userService.startsWith(firstName);
        List<UserDTO> userDTOS = new ArrayList<>();
        users.stream().forEach(user -> {
            userDTOS.add(new UserDTO(user));
        });
        return userDTOS;
    }

    @GetMapping("endsWith/{firstName}")
    public List<UserDTO> endsWith(@PathVariable String firstName) {
        List<User> users = userService.endsWith(firstName);
        List<UserDTO> userDTOS = new ArrayList<>();
        users.stream().forEach(user -> {
            userDTOS.add(new UserDTO(user));
        });
        return userDTOS;
    }

    @GetMapping("/getByCity/{city}")
    public List<UserDTO> getByCity(@PathVariable String city) {
        List<User> users = userService.getByCity(city);
        List<UserDTO> userDTOS = new ArrayList<>();
        users.stream().forEach(user -> {
            userDTOS.add(new UserDTO(user));
        });

        return userDTOS;
    }
}
