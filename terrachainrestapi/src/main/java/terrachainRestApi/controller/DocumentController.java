package terrachainRestApi.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/users/")
public class DocumentController {

    @GetMapping("documents")
    public String getDocuments(){
        return "DocumentsList";
    }
}
