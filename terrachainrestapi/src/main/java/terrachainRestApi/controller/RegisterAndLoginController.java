package terrachainRestApi.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import terrachainRestApi.dtos.JWTAuthDTO;
import terrachainRestApi.dtos.LoginDTO;
import terrachainRestApi.dtos.SignUpDTO;
import terrachainRestApi.entity.Role;
import terrachainRestApi.entity.User;
import terrachainRestApi.repository.RoleRepository;
import terrachainRestApi.repository.UserRepository;
import terrachainRestApi.security.JwtTokenProvider;

import java.util.Collections;

@RestController
@RequestMapping("/api/auth")
public class RegisterAndLoginController {

    private AuthenticationManager authenticationManager;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;
    private JwtTokenProvider tokenProvider;

    public RegisterAndLoginController(final AuthenticationManager authenticationManager,
                                      final UserRepository userRepository,
                                      final RoleRepository roleRepository,
                                      final PasswordEncoder passwordEncoder,
                                      final JwtTokenProvider tokenProvider) {

        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping("/signin")
    public ResponseEntity<String> authenticateUser(@RequestBody LoginDTO loginDTO) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginDTO.getUsernameOrEmail(), loginDTO.getPassword()
                ));
//
//        SecurityContextHolder.getContext()
//                .setAuthentication(authentication);
//        String token = tokenProvider
//                .generateToken(authentication);
//
//        return ResponseEntity.ok(new JWTAuthDTO(token));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return new ResponseEntity<>("User signed-in successfully!.", HttpStatus.OK);

    }

    @PostMapping("/signup")
    public ResponseEntity<?> registerUser(@RequestBody SignUpDTO signUpDTO) {
        if (userRepository.existsByUsername(signUpDTO.getUsername())) {
            return new
                    ResponseEntity<>("Username is already taken!", HttpStatus.BAD_REQUEST);
        }

        if (userRepository.existsByEmail(signUpDTO.getEmail())) {
            return new
                    ResponseEntity<>("Email is already taken!", HttpStatus.BAD_REQUEST);
        }

        User user = new User();
        user.setFirstName(signUpDTO.getName());
        user.setLastName(signUpDTO.getLastName());
        user.setUsername(signUpDTO.getUsername());
        user.setEmail(signUpDTO.getEmail());
        user.setPassword(passwordEncoder
                .encode(signUpDTO.getPassword()));
//        user.setAddress(signUpDTO.getAddress());

        Role role = roleRepository.findByName("ROLE_ADMIN").get();
        user.setUserRole(Collections.singleton(role));

        userRepository.save(user);

        return new
                ResponseEntity<>("User registered successfully.", HttpStatus.OK);
    }
}
