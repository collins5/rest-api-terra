package terrachainRestApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@ComponentScan({"terrachainRestApi.controller","terrachainRestApi.service"})
//@EntityScan("terrachainRestApi.entity")
//@EnableJpaRepositories("terrachainRestApi.repository")
//@EnableWebMvc
public class TerrachainrestapiApplication {
	public static void main(String[] args) {
		SpringApplication.run(TerrachainrestapiApplication.class, args);
	}

}
