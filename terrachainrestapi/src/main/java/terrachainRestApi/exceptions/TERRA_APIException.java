package terrachainRestApi.exceptions;

import org.springframework.http.HttpStatus;

public class TERRA_APIException  extends RuntimeException {

    private HttpStatus status;
    private String message;

    public TERRA_APIException(final HttpStatus status,
                              final String message) {
        this.status = status;
        this.message = message;
    }

    public TERRA_APIException(String message,
                              HttpStatus status, String message_01){
        this.status = status;
        this.message = message_01;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
