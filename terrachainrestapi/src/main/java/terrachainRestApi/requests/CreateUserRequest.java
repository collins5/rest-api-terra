package terrachainRestApi.requests;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Set;

@Getter
@Setter
public class CreateUserRequest {

    @NotBlank(message = "First name is required")
    @Size(min = 2, message = "Firstname should have at least 2 characters")
    private String firstName;

    @NotBlank(message = "Last name is required")
    @Size(min = 2, message = "Lastname should have at least 2 characters")
    private String lastName;

    @NotBlank(message = "Username is required")
    @Size(min = 2, message = "Username should have at least 2 characters")
    private String username;

    @NotEmpty(message = "Email should not be null or empty")
    @Email
    private String email;

    @NotBlank(message = "Password is required")
    private String password;

    private String street;

    private String city;

    private String provinz;

    private String country;

    private Set<CreateDocumentRequest> documents;


}
