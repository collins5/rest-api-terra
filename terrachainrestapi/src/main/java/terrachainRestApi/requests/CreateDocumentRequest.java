package terrachainRestApi.requests;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
public class CreateDocumentRequest {

    @NotEmpty(message = "Name should not be null or empty")
    private String name;
    @NotEmpty(message = "EntryDate should not be null or empty")
    private Date entryDate;
    @NotEmpty(message = "UpdateDate should not be null or empty")
    private Date updateDate;
    @NotEmpty(message = "ExpirationDate should not be null or empty")
    private Date expirationDate;
    private Boolean locked;
    @Size(min = 10, message = "Comment must be minimum 10 characters")
    private String comment;

}
